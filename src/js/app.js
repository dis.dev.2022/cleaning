"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import tabs from './components/tabs.js'; // Tabs
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import Spoilers from "./components/spoilers.js";

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Вкладки (tabs)
tabs();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Spoilers
Spoilers();


// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

document.addEventListener('click', MediaContent);

function MediaContent(event) {
    if (event.target.closest('[data-media-button]')) {
        const media = event.target.closest('[data-media]');
        const mediaContent = media.querySelector('[data-media-content]');

        mediaContent.classList.toggle('open');
    }
}
